const Model = require('./model')


class User extends Model {
    constructor() {
        super('users')
    }

    get(cb) {
        this.dm.read((users) =>
        { 
            users=users.filter(item => item.is_delete != "true")
            cb(users)
        })
    }
    getByName(name,cb) {
        this.dm.read((users) =>
        { 
            users=users.filter(item => item.is_delete != "true" && item.name == name)
            cb(users)
        })
    }
    add(user, cb) {
        let data = {
            name: user.name,
            email: user.email,
            password: user.password,
           
        }

        this.dm.add(data, (user) => cb(user))
    }

    edit(id, newUser, cb) {
        this.findByID(id, (user) => {
            if(!user) {
                res.status(404).json({message:'User not found'})
            }else{

            user = this.updateObj(user, newUser)

            this.dm.update(id, user, (user) => cb(user))
            }
        })
        
    }

    delete(id, cb) {
        this.dm.delete(id, () => cb())
    }

    
}

module.exports = new User