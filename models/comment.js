const Model = require('./model')

class Comment extends Model {

  constructor() {
    super('Comments')
}
get(cb) {
  this.dm.read((Comments) =>{ 
    Comments = Comments.filter(item => item.is_delete != "true")
    cb(Comments)
  })
}

getCountComment(id,cb){
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
  this.dm.read((Comments) =>{ 
    Comments = Comments.filter(item => item.is_delete != "true"  && item.note_id ==id)
    var i=0;
    Comments.forEach(element=>{
      i++;
    })
    resolve(i)
  })
})
})
return promise
}
getComment(id,cb){
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
  this.dm.read((Comments) =>{ 
    Comments = Comments.filter(item => item.is_delete != "true"  && item.note_id ==id)
    
    resolve(Comments)
  })
})
})
return promise
}
add(Comment, cb) {
        Comment.date = new Date()

        this.dm.add(Comment, (Comment) => cb(Comment))
    }
    
    edit(id, newComment, cb) {
      this.findByID(id, (Comment) => {
          if(!Comment) {
             res.status(200).json({message:'Comment not found'})
          }
          else{
          Comment = this.updateObj(Comment, newComment)

          this.dm.update(id, Comment, (Comment) => cb(Comment))
          }
      })
  }

  delete(id, cb) {
      this.dm.delete(id, () => cb())
  }
  

};
module.exports = new Comment
