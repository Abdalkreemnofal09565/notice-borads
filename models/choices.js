const Model = require('./model')

class Choice extends Model {

  constructor() {
    super('Choices')
}
get(cb) {
  this.dm.read((Choices) =>{ 
    Choices = Choices.filter(item => item.is_delete != "true")
    cb(Choices)
  })
}
getTask(id,cb) {
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
  this.dm.read((Choices) =>{ 
    var Choices_ended = Choices.filter(item => item.is_delete != "true" && item.note_id ==id && item.status==1)
    var Choices_all = Choices.filter(item => item.is_delete != "true" && item.note_id ==id)
    var i=0;
    Choices_ended.forEach(element=>{
      i++;
    })
    var j=0;
    Choices_all.forEach(element=>{
      j++;
    })
    var data= {"finished_task":i,"all_task" :j}
    resolve(data)
  })
})
})
return promise
}
getChoices(id,cb) {
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
  this.dm.read((Choices) =>{ 
    var Choices_ended = Choices.filter(item => item.is_delete != "true" && item.note_id ==id && item.status==1)
    var Choices_all = Choices.filter(item => item.is_delete != "true" && item.note_id ==id)
   
   
    resolve(Choices_all)
  })
})
})
return promise
}
add(Choice, cb) {
        Choice.date = new Date()

        this.dm.add(Choice, (Choice) => cb(Choice))
    }
    
    edit(id, newChoice, cb) {
      this.findByID(id, (Choice) => {
          if(!Choice) {
             res.status(200).json({message:'Choice not found'})
          }
          else{
          Choice = this.updateObj(Choice, newChoice)

          this.dm.update(id, Choice, (Choice) => cb(Choice))
          }
      })
  }

  delete(id, cb) {
      this.dm.delete(id, () => cb())
  }
  

};
module.exports = new Choice
