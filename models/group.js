const Model = require('./model')
const note = require('../models/note');
const user = require('./user')

class Group extends Model {
    constructor() {
        super('groups')
    }

    get(cb) {
        this.dm.read((groups) =>{ 
            groups=groups.filter(item => item.is_delete != "true")
            cb(groups)
        })
    }

    add(group, cb) {
        this.dm.add(group, (group) => cb(group))
    }

    edit(id, newGroup, cb) {
        this.findByID(id, (group) => {
            if(!group) {
                res.status(404).json({message:'Group not found'})
            }else{
            group = this.updateObj(group, newGroup)

            this.dm.update(id, group, (group) => cb(group))
            }
        })
    }

    delete(id, cb) {
        this.dm.delete(id, () => cb())
    }

    getByuserBoradID(id,cb){
        const promise=new Promise((resolve,reject)=>{
            setTimeout(()=>{
                var grs=new Array()
        this.dm.read(async (groups) =>{ 
            groups=groups.filter(item => item.is_delete != "true" && item.user_borad_id ==id )
            if(groups.length)
            { 
            //   console.log('groups',groups)
                for(let i = 0 ; i < groups.length; i++){
                        var group={}
                        // console.log("i : ",i,"     ",groups.length)
                        let notes = await note.getOneByGroupID(groups[i].id)
                        // console.log('notes ended : ',notes)
                        // console.log(groups[i]);
                        group.name=groups[i].name;
                        group.id=groups[i].id;
                        group.notes=notes
                           
                        grs.push(group)
                        if(i==groups.length-1){
                            // console.log(grs)
                            resolve(grs)
                        }
                     
                    }
               
            //   await Promise.all(arrayOfPromises).then((value)=>{console.log("one ",value)})

              } 
              else{
                  resolve([])
              }
            
        })
    })
})
return promise
    }
    getoneByuserBoradID  (id,cb){
        const promise=new Promise((resolve,reject)=>{
            setTimeout(()=>{
        var grs=new Array()
             
        this.dm.read(async (groups) =>{ 
            groups=groups.filter(item => item.is_delete != "true" && item.user_borad_id ==id )
            
                    if(groups.length)
                    { 
                    //   console.log('groups',groups)
                        for(let i = 0 ; i < groups.length; i++){
                                var group={}
                                // console.log("i : ",i,"     ",groups.length)
                                let notes = await note.getOneByGroupID(groups[i].id)
                                // console.log('notes ended : ',notes)
                                // console.log(groups[i]);
                                group.name=groups[i].name;
                                group.id=groups[i].id;
                                group.notes=notes
                                   
                                grs.push(group)
                                if(i==groups.length-1){
                                    console.log('here')
                                    resolve(grs)
                                }
                             
                            }
                       
                    //   await Promise.all(arrayOfPromises).then((value)=>{console.log("one ",value)})

                      } 
                      else{
                          resolve([])
                      }
        })
    }) 
})
return promise
    }
   
    // users(id, cb) {
    //     this.findByID(id, (group) => {
    //         if (group.users.length == 0){
    //             return cb([])
    //         }else{
    //         let itemProcessed = 0
    //         let users = []

    //         group.users.forEach(userID => {
    //             user.findByID(userID, (user) => {
    //                 itemProcessed++

    //                 users.push(user)

    //                 if (itemProcessed == group.users.length) cb(users)
                
    //             })
    //         })
    //        }
    //     })
    // }
}

module.exports = new Group