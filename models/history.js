const Model = require('./model')


class History extends Model {
    constructor() {
        super('history')
    }

    get(cb) {
        this.dm.read((historys) => cb(historys))
    }
    getByUserID(id ,cb){
      
        this.dm.read((historys) =>{ 
            historys=historys.filter(item => item.is_delete != "true" && item.user_id ==id )
            cb(historys)
        })
    }
    add(history, cb) {
        this.dm.add(history, (history) => cb(history))
    }
    
    edit(id, newHistory, cb) {
        this.findByID(id, (history) => {
            if(!history) {
                res.status(404).json({message:'History not found'})
            }
            else{
            history = this.updateObj(history, newHistory)

            this.dm.update(id, history, (history) => cb(history))
           }
        })
    }

    delete(id, cb) {
        this.dm.delete(id, () => cb())
    }

    deleteBy(key, value, cb) {
        this.findBy(key, value, (history) => {
            if(history)
                this.delete(history.id, () => cb())
            else
                cb()
        })
    }
}

module.exports = new History