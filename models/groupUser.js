const Model = require('./model')

const user = require('./user')

class GroupUser extends Model {
    constructor() {
        super('GroupUsers')
    }

    get(cb) {
        this.dm.read((GroupUsers) => cb(GroupUsers))
    }

    add(GroupUser, cb) {
        this.dm.add(GroupUser, (GroupUser) => cb(GroupUser))
    }

    edit(id, newGroupUser,GroupUser, cb) {
       
            GroupUser = this.updateObj(GroupUser, newGroupUser)

            this.dm.update(id, GroupUser, (GroupUser) => cb(GroupUser))
        
    }

    delete(id, cb) {
        this.dm.delete(id, () => cb())
    }

    users(id, cb) {
        this.findByID(id, (GroupUser) => {
            if (GroupUser.users.length == 0)
            {
                return cb([])
            }else{
                let itemProcessed = 0
                let users = []
    
                GroupUser.users.forEach(userID => {
                    user.findByID(userID, (user) => {
                        itemProcessed++
    
                        users.push(user)
    
                        if (itemProcessed == GroupUser.users.length){ cb(users)}
                    })
                })
            }
            
        })
    }
}

module.exports = new GroupUser