const Model = require('./model')
const Group = require('./group')
const User = require('./user')

const { request } = require('express')

class groupRequest extends Model {
    constructor() {
        super('group-requests')
    }

    get(cb) {
        this.dm.read((groupRequests) => cb(groupRequests))
    }

    add(groupRequest,group, cb) {
        console.log(groupRequest)
        var groupID = groupRequest.groupID

        

        
            var data = {
                userID: groupRequest.userID,
                groupID: parseInt(groupRequest.groupID),
                adminID: group.adminID,
                accepted: null
            }
    
            this.dm.add(data, (groupRequest) => cb(groupRequest))
        
    
    }

    edit(id, newGroupRequest,groupRequest, cb) {
        
                groupRequest = this.updateObj(groupRequest, newGroupRequest)

                this.dm.update(id, groupRequest, (groupRequest) => cb(groupRequest))
             
            
        
    }

    delete(id, cb) {
        this.dm.delete(id, () => cb())
    }

    usersInfo(requests, cb) {
        if (requests.length == 0)
        {
            return cb([])
        }else{
            let itemProcessed = 0

            requests.forEach(request => {
                User.findByID(request.userID, (user) => {
                    User.findByID(request.adminID, (admin) => {
                        itemProcessed++
    
                        request.admin = admin
                        request.user = user
    
                        if (itemProcessed == requests.length) cb(requests)
                    })
                })
            })
        }
            
        
        


    }
}

module.exports = new groupRequest