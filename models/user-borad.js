const Model = require('./model')

const user = require('../models/user');
const group = require('../models/group');
const comment = require('../models/comment');
const note = require('../models/note');
const choice = require('../models/choices');
const borad = require('../models/borad');
class userBorad extends Model {
    constructor() {
        super('user-borad')
    }

    get(cb) {
        this.dm.read((userBorads) =>
        { 
            userBorads=userBorads.filter(item => item.is_delete != "true")
            cb(userBorads)
        })
    }
    getMemberBorad(borad_id,cb) {
        this.dm.read((userBorads) =>
        { 
            userBorads=userBorads.filter(item => item.is_delete != "true" && item.borad_id==borad_id)
            cb(userBorads)
        })
    }
    async getByBoradID(BoradID,cb) {
        const promise=new Promise((resolve,reject)=>{
            setTimeout(()=>{
                
        this.dm.read((userBorads) =>
        { 
            userBorads=userBorads.filter(item => item.is_delete != "true" && item.borad_id ==BoradID )
           
            if(userBorads.length){
                let j = 0
                var data=new Array();
                userBorads.forEach(el_userBorad=>{
                    
                group.getByuserBoradID(el_userBorad.id).then((groups)=>{
                   
                    j++
                  if(groups.length)
                  { 
                   
                    
                if(j==userBorads.length){
                      console.log(groups)
                    resolve(groups)
                  }
                   

                    //   groups.forEach(el_group=>{
                        
                        // note.getByGroupID(el_group.id).then((notes)=>{
                            
                        //     i++
                        //     data.push(notes)
                        //   if ( j == userBorads.length && i == groups.length )
                          
                                
                               
                            //  })
                             
                    //   })
                  
                    } else {
                        if (j == userBorads.length)
                            resolve([])
                    } 
                  })
                })
              } else {
                  resolve([])
              }
            
        })
    })
   })
   return promise  
    }
    async getOneByBoradID(BoradID,cb) {
        const promise=new Promise((resolve,reject)=>{
            setTimeout(()=>{
                
        this.dm.read((userBorads) =>
        { 
            userBorads=userBorads.filter(item => item.is_delete != "true" && item.borad_id ==BoradID )
            if(userBorads.length){
                let j = 0
                var data=new Array();
               
                userBorads.forEach(el_userBorad=>{
                //    return resolve(1)
                group.getoneByuserBoradID(el_userBorad.id).then((groups)=>{
                    j++
                   
                  if(j==userBorads.length)
                    resolve(groups)
                  
                  })
                })
              } else {
                resolve([])
              }
            
        })
    })
   })
   return promise  
    }
    async getByUserID(UserID,cb) {
        const promise=new Promise((resolve,reject)=>{
            setTimeout(()=>{
                
        this.dm.read((userBorads) =>
        { 
            userBorads=userBorads.filter(item => item.is_delete != "true" && item.user_id ==UserID )
           
            if(userBorads.length){
                resolve(userBorads)
            }else{
                resolve([])
            }
            
        })
    })
   })
   return promise  
    }
    add(userBorad, cb) {
        let data = {
            user_id: userBorad.user_id,
            borad_id: userBorad.borad_id,
           role: userBorad.role
           
        }

        this.dm.add(data, (userBorad) => cb(userBorad))
    }

    edit(id, newuserBorad, cb) {
        this.findByID(id, (userBorad) => {
            if(!userBorad) {
                res.status(404).json({message:'userBorad not found'})
            }else{

            userBorad = this.updateObj(userBorad, newuserBorad)

            this.dm.update(id, userBorad, (userBorad) => cb(userBorad))
            }
        })
        
    }

    delete(id, cb) {
        this.dm.delete(id, () => cb())
    }

    
}

module.exports = new userBorad