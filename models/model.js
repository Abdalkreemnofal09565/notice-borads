const DM = require('../util/dataManager');

module.exports = class Model {
    constructor(file) {
        this.file = file
        this.dm = new DM(file)
    }

    findByID(id, cb) {
        
        this.dm.read(items => {
            let foundItem = null

            items.forEach(item => {
                if(item.id == id && item.is_delete!="true") {
                    foundItem = item
                }
            })

            cb(foundItem)
        })
    }

    findBy(key, value, cb) {
        this.dm.read(items => {
            let foundItem = null

            items.forEach(item => {
                if(item[key] == value && item["is_delete"]!="true") {
                    foundItem = item
                }
            })

            cb(foundItem)
        })
    }

    updateObj(oldObj, newObj) {
        for(let key in newObj) {
            oldObj[key] = newObj[key]
        }

        return oldObj
    }
}