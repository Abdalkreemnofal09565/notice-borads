const Model = require('./model')


class userGroup extends Model {
    constructor() {
        super('user-Group')
    }

    get(cb) {
        this.dm.read((userGroups) =>
        { 
            userGroups=userGroups.filter(item => item.is_delete != "true")
            cb(userGroups)
        })
    }
    getByGroupID(GroupID,cb) {
        this.dm.read((userGroups) =>
        { 
            userGroups=userGroups.filter(item => item.is_delete != "true" && item.Group_id ==GroupID )
            cb(userGroups)
        })
    }
    add(userGroup, cb) {
       

        this.dm.add(userGroup, (userGroup) => cb(userGroup))
    }

    edit(id, newuserGroup, cb) {
        this.findByID(id, (userGroup) => {
            if(!userGroup) {
                res.status(404).json({message:'userGroup not found'})
            }else{

            userGroup = this.updateObj(userGroup, newuserGroup)

            this.dm.update(id, userGroup, (userGroup) => cb(userGroup))
            }
        })
        
    }

    delete(id, cb) {
        this.dm.delete(id, () => cb())
    }

    getMemberGroup(group_id,cb) {
        
        this.dm.read((userGroup) =>
        { 
            userGroup=userGroup.filter(item => item.is_delete != "true" && item.group_id==group_id)
            cb(userGroup)
        })
    }

    
}

module.exports = new userGroup