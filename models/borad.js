const Model = require('./model')
const userBorad =require('../models/user-borad');
const user = require('../models/user');
const group = require('../models/group');
const comment = require('../models/comment');
const choice = require('../models/choices');
class Borad extends Model {

  constructor() {
    super('borads')
}
get(cb) {
 const promise=new Promise((resolve,reject)=>{
   setTimeout(()=>{
    this.dm.read((borads) =>{ 
      borads = borads.filter(item => item.is_delete != "true" && item.type!=3 && item.group_id==null)
     
      resolve(borads)
    })
   })
 

 })
  return promise
}
async getByBoradID(borad_id,cb){
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
     this.dm.read((borad) =>{ 
       borad= borad.filter(item => item.is_delete != "true" && item.id== borad_id )
                resolve(borad)
              })   
           })   
       })  

   return promise
}
async getBoradDetails(id,cb) {
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
     this.findByID(id,(borads) =>{ 
       
       
       var data=new Array();
       var borad={}
       borad.id=borads.id;
       borad.name=borads.name;
      
       userBorad.getOneByBoradID(borads.id).then((userBorads)=>{
         
             borad.groups=userBorads;
          
         
             data.push(borad)
             
   
            
               resolve(data)
     
             
           
         
       })
      
     
       
     })
    })
  
 
  })
   return promise
 }
 async  getBoradsDetails(cb) {
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
     this.dm.read((borads) =>{ 
       borads = borads.filter(item => item.is_delete != "true" && item.type!=3 && item.group_id==null)
       var data=new Array();
       let i = 0
     
       borads.forEach(elemnt => {
       
        var borad={}
        borad.id=elemnt.id;
        borad.name=elemnt.name;
       
        userBorad.getByBoradID(elemnt.id).then((userBorads)=>{
          
              borad.groups=userBorads;
              i++ 
             console.log()
              data.push(borad)
              
    
              if(i == borads.length) {
               console.log(data)
                resolve(data)
      
              }
            
          
        })
       
        
        
         
       })  
     })
    })
  
 
  })
   return promise
 }
 async getUserGroupBorads(id,cb) {
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
     this.dm.read((borads) =>{ 
       borads = borads.filter(item => item.is_delete != "true" && item.type!=3 && item.group_id==id)
       console.log(borads)
       if(borads.length){
        var data=new Array();
        let i = 0
      
        borads.forEach(elemnt => {
        
         var borad={}
         borad.id=elemnt.id;
         borad.name=elemnt.name;
        
         userBorad.getByBoradID(elemnt.id).then((userBorads)=>{
           
               borad.groups=userBorads;
               i++ 
              console.log()
               data.push(borad)
               
     
               if(i == borads.length) {
                console.log(data)
                 resolve(data)
       
               }
              })
            })
       }else{
        resolve([])
       }
      
            
          
       
       
        
        
         
     
     })
    })
  
 
  })
   return promise
 }
add(borad, cb) {
        borad.date = new Date()

        this.dm.add(borad, (borad) => cb(borad))
    }
    
    edit(id, newBorad, cb) {
      this.findByID(id, (borad) => {
          if(!borad) {
             return res.json({message:'Borad not found'});
          }
          else{
          borad = this.updateObj(borad, newBorad)

          this.dm.update(id, borad, (borad) => cb(borad))
          }
      })
  }

  delete(id, cb) {
      this.dm.delete(id, () => cb())
  }
  

};
module.exports = new Borad
