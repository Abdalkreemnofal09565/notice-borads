const Model = require('./model')
const userBorad =require('../models/user-borad');
const user = require('../models/user');
const group = require('../models/group');
const comment = require('../models/comment');
const choice = require('../models/choices');

class Note extends Model {

  constructor() {
    super('Notes')
}
get(cb) {
  this.dm.read((Notes) =>{ 
    Notes = Notes.filter(item => item.is_delete != "true")
    cb(Notes)
  })
}
getnotes(el_note,cb){
  
   
    var note={}
    note.id=el_note.id;
    note.name=el_note.name;
    const promise=new Promise((resolve,reject)=>{
      setTimeout(()=>{
     comment.getCountComment(el_note.id).then((comments)=>{
     
      note.comments=comments;
      choice.getTask(el_note.id).then(
        (choices)=>{
          
         
          note.choices=choices
         
          user.findByID(el_note.id,(users)=>{
            if(users.length){
              note.user_id=users.id;
            note.name_user=users.name;
            }
            
          })
         
          resolve(note)
        }) 
    })
    
      
     
     }) 
    
    })
    return promise
    
    
 

}
getOneNotes(el_note,cb){
  
   
  var note={}
  note.id=el_note.id;
  note.name=el_note.name;
  note.file=el_note.file;
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
   comment.getComment(el_note.id).then((comments)=>{
   
    note.comments=comments;
    choice.getChoices(el_note.id).then(
      (choices)=>{
        
       
        note.choices=choices
       
        // user.findByID(el_note.id,(users)=>{
        //   if(users.length){
        //     note.user_id=users.id;
        //   note.name_user=users.name;
        //   }
          
        // })
       
        resolve(note)
      }) 
  })
  
    
   
   }) 
  
  })
  return promise
  
  


}
getByGroupID(id,cb){
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
   
      var Anotes=new Array();
  this.dm.read((notes) =>{ 
    notes=notes.filter(item => item.is_delete != "true" && item.group_id ==id )
               
                    if(notes.length)
                          {
                            let i = 0
                            notes.forEach(el_note=>{
                              this.getnotes(el_note).then(re=>{
                           
                                i++
                                Anotes.push(re)
                                if(i == notes.length) 
                                {
                                 
                                  resolve(Anotes)
                                }
                                

                              })
                              
                            })
                         
                            
                           
                          }
                          else{
                            resolve([])
                          }
                          
})
})
})
return promise
}
getOneByGroupID(id,cb){
  const promise=new Promise((resolve,reject)=>{
    setTimeout(()=>{
   
      var Anotes=new Array();
  this.dm.read((notes) =>{ 
    notes=notes.filter(item => item.is_delete != "true" && item.group_id ==id )
               
                    if(notes.length)
                          {
                            let i = 0
                            notes.forEach(el_note=>{
                              this.getOneNotes(el_note).then(re=>{
                           
                                i++
                                Anotes.push(re)
                                if(i == notes.length) 
                                {
                                 
                                  resolve(Anotes)
                                }
                                

                              })
                              
                            })
                         
                            
                           
                          }
                          else{
                            resolve([])
                          }
                          
})
})
})
return promise
}
add(Note, cb) {
        Note.date = new Date()

        this.dm.add(Note, (Note) => cb(Note))
    }
    
    edit(id, newNote, cb) {
      this.findByID(id, (Note) => {
          if(!Note) {
             res.status(200).json({message:'Note not found'})
          }
          else{
          Note = this.updateObj(Note, newNote)

          this.dm.update(id, Note, (Note) => cb(Note))
          }
      })
  }

  delete(id, cb) {
      this.dm.delete(id, () => cb())
  }
  

};
module.exports = new Note
