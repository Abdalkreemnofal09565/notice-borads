const Model = require('./model')
const Borad = require('./borad')
const User = require('./user')

const { request } = require('express')

class BoradRequest extends Model {
    constructor() {
        super('Borad-requests')
    }

    get(cb) {
        this.dm.read((BoradRequests) => cb(BoradRequests))
    }
    

    add(BoradRequest,Borad, cb) {
        
        var BoradID = BoradRequest.BoradID

        

        console.log(BoradRequest)
            var data = {
                userID: BoradRequest.userID,
                BoradID: parseInt(BoradRequest.boradID),
                adminID: Borad.adminID,
                accepted: null
            }
            
            this.dm.add(data, (BoradRequest) => cb(BoradRequest))
        
    
    }

    edit(id, newBoradRequest,BoradRequest, cb) {
        
                BoradRequest = this.updateObj(BoradRequest, newBoradRequest)

                this.dm.update(id, BoradRequest, (BoradRequest) => cb(BoradRequest))
             
            
        
    }

    delete(id, cb) {
        this.dm.delete(id, () => cb())
    }

    usersInfo(requests, cb) {
        if (requests.length == 0)
        {
            return cb([])
        }else{
            let itemProcessed = 0

            requests.forEach(request => {
                User.findByID(request.userID, (user) => {
                    User.findByID(request.adminID, (admin) => {
                        itemProcessed++
    
                        request.admin = admin
                        request.user = user
    
                        if (itemProcessed == requests.length) cb(requests)
                    })
                })
            })
        }
            
        
        


    }
}

module.exports = new BoradRequest