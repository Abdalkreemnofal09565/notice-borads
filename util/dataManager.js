const fs = require('fs')

function getID(filename, cb) {
    new DataManager().file(filename).read((items) => {
        if(items.length == 0)
            return cb(1)
        
        return cb(items[items.length - 1].id + 1)
    })
}

class DataManager {
    constructor(filename) {
        this.currentFile = filename
    }

    file(filename) {
        this.currentFile = filename
        return this
    }

    read(cb) {
        fs.readFile(`./data/${this.currentFile}.json`, (err, content) => {
            if(!err && content.toString())
            {
                cb(JSON.parse(content))
            }
            else
            {
                cb([])
            }
               
        })
    }

    write(data, cb) {
        fs.writeFile(`./data/${this.currentFile}.json`, JSON.stringify(data), (err) => {
            if(err) 
                throw new Error(err)
            else
                cb(data)
        })
    }

    add(item, cb) {
        this.read(items => {
            getID(this.currentFile, id => {
                item.id = id
                item.is_delete="false";
                item.created_at=new Date();
                item.updated_at=new Date();
                items.push(item)
    
                this.write(items, () => cb(item))
            })
        })
        
    }

    update(id, newItem, cb) {
        this.read(items => {
            items = items.map(item => {
                if(item.id == id) 
                {
                    newItem.is_delete="false";
                    newItem.updated_at=new Date();
                    return newItem
                }
                return item
            })

            this.write(items, () => cb(newItem))
        })
    }

    delete(id,  cb) {
        this.read(items => {
            items = items.map(item => {
                if(item.id == id) 
                {
                    item.is_delete="true";
                    item.updated_at=new Date();
                    return item
                }
                return item
            })

            this.write(items, () => cb(items))
        })
    }

}

module.exports = DataManager