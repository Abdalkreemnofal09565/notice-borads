const Note = require('../models/note');
const Group = require('../models/Group');
const Comment = require('../models/Comment');
const History = require('../models/history')



const Choices = require('../models/Choices');

exports.get= (req, res, next) => {
  Note.get((notes) => {
     
     res.status(200).json({message:'',data: notes})
  })
},

exports.deleteNote = (req, res, next) => {
  
   const id = req.params.NoteID

  Note.findByID(id, (note) => {
      if(!note) {
          res.status(404).json({message:'Note not found'})
      }
      else {
        
      
              Note.delete(id, (note) => {
                Datahistory={
                  the_id: id,
                  action:"delete",
                  type:"notes",
                  user_id:req.userID,
                  
                }
                History.add(Datahistory,()=>{})
                 res.status(200).json({message:'Note has been deleted'})
              })
         
        }
  })
  
};
exports.deleteComment = (req, res, next) => {
  
  const id = req.params.CommentID

  comment.findByID(id, (comment) => {
     if(!comment) {
         res.status(404).json({message:'Note not found'})
     }
     else {
     
      comment.delete(id, (comment) => {
        Datahistory={
          the_id:Comment.id,
          action:"delete",
          type:"comment",
          user_id:req.userID,
          
        }
        History.add(Datahistory,()=>{})
                res.status(200).json({message:'Note has been deleted'})
             })
        
       }
 })
 
};
exports.deleteChoices = (req, res, next) => {
  
  const id = req.params.ChoicesID

  Choices.findByID(id, (Choices) => {
     if(!Choices) {
         res.status(404).json({message:'Note not found'})
     }
     else {
     

      Choices.delete(id, (Choices) => {
        Datahistory={
          the_id:id,
          action:"delete",
          type:"choices",
          user_id:req.userID,
          
        }
        History.add(Datahistory,()=>{})
                res.status(200).json({message:'Note has been deleted'})
             })
        
       }
 })
 
};

exports.editNote = (req, res, next) => {
  const id = req.params.id

  const NoteData = {
      name: req.body.name,
     
  }

  
  Note.edit(id, NoteData, (Note) => {
  
              res.status(200).json({message:'Note has been updated', data: Note})
    
      })
  
};
exports.addNote =(req, res, next)=> {
  const NoteData = {
      name: req.body.name,
      group_id: req.body.group_id,
      check_list_name: req.body.check_list_name,
      file: req.file ? '/files/' + req.file.filename : '',
      user_id:req.userID
  }
  
  Group.findByID(req.body.group_id,(group)=>{
    if(!group){
      res.status(404).json({message:"The group not found "});
    }else{
      Note.add(NoteData, (Note) => {
        Datahistory={
          the_id: Note.id,
          action:"add",
          type:"notes",
          user_id:req.userID,
          
        }
        History.add(Datahistory,()=>{})
        res.status(200).json({message:'Note has been created', data:Note})
             })
    }
    
  })
  

}
exports.addComment =(req, res, next)=> {
  const CommentData = {
      content: req.body.content,
      note_id: req.body.note_id,
      user_id:req.userID
  }
  
  Note.findByID(req.body.note_id,(note)=>{
    if(!note){
      res.status(404).json({message:"The note not found "});
    }else{
      Comment.add(CommentData, (Comment) => {
        Datahistory={
          the_id:Comment.id,
          action:"add",
          type:"comment",
          user_id:req.userID,
          
        }
        History.add(Datahistory,()=>{})
        res.status(200).json({message:'comment has been created', data:Comment})
             })
    }
    
  })
  

}
exports.addChoices =(req, res, next)=> {
  const ChoicesData = {
      name: req.body.name,
      note_id: req.body.note_id,
      status:req.body.status
  }
  
  Note.findByID(req.body.note_id,(note)=>{
    if(!note){
      res.status(404).json({message:"The note not found "});
    }else{
      Choices.add(ChoicesData, (Choices) => {
        Datahistory={
          the_id:Choices.id,
          action:"add",
          type:"choices",
          user_id:req.userID,
          
        }
        History.add(Datahistory,()=>{})
        res.status(200).json({message:'Choices has been created', data:Choices})
             })
    }
    
  })
  

}
exports.editNameChoices =(req, res, next)=> {
  const ChoicesData = {
    name: req.body.name,
    
}
Choices.findByID( req.params.choiceID,(choices)=>{
  if(!choices)
    {
           res.status.json({message:"the choice not found "})
    }else{
      Choices.edit(req.params.choiceID,ChoicesData,(choice)=>{
        Datahistory={
          the_id:req.params.choiceID,
          action:"edit name",
          type:"choices",
          user_id:req.userID,
          
        }
        History.add(Datahistory,()=>{})
        res.status(200).json({message:"the name has changed"})
      })
    }
})
}
exports.editStatusChoices =(req, res, next)=> {
  const ChoicesData = {
    status: req.body.status
    
}
Choices.findByID( req.params.choiceID,(choices)=>{
  if(!choices)
    {
           res.status.json({message:"the choice not found "})
    }else{
      Choices.edit(req.params.choiceID,ChoicesData,(choice)=>{
        Datahistory={
          the_id:req.params.choiceID,
          action:"edit status",
          type:"choices",
          user_id:req.userID,
          
        }
        History.add(Datahistory,()=>{})
        res.status(200).json({message:"the status has changed"})
      })
    }
})
}
exports.changeNotePlace =(req, res, next)=> {
  
    const NoteData={ group_id: req.body.group_id}
   
  Group.findByID(req.body.group_id,(group)=>{
    if(!group){
      res.status(404).json({message:"The group not found "});
    }else{
      Note.edit(req.body.note_id,NoteData, (Note) => {
        Datahistory={
          the_id: req.body.note_id,
          action:"replace Note",
          type:"notes",
          user_id:req.userID,
          
        }
        History.add(Datahistory,()=>{})
        res.status(200).json({message:'Note has been replaced group', data:Note})
             })
    }
    
  })
}
exports.uploadFile =(req, res, next)=> {
  
  const NoteData={ }
  if(req.file)
     NoteData.file = '/files/' + req.file.filename

    Note.edit(req.body.note_id,NoteData, (Note) => {
     
      res.status(200).json({message:'upload the file success', data:Note})
           })




};
exports.changeNoteCheckList =(req, res, next)=> {
  
  const NoteData={ check_list_name: req.body.name}
 

    Note.edit(req.params.id,NoteData, (Note) => {
     
      Datahistory={
        the_id: req.params.id,
        action:"change name checklist",
        type:"notes",
        user_id:req.userID,
        
      }
      History.add(Datahistory,()=>{})
      res.status(200).json({message:'Note has been change checklist', data:Note})
           })
  
  

}
exports.getNote = (req, res, next) => {
  
  if (req.params.NoteID) {
    
          Note.get((Notes) => {
          Notes = Notes.filter(Note => Note.id == req.params.NoteID )

            res.status(200).json({message:'', data:Notes})
        })
   
}
else{
  res.status(404).json({message:'must have the Note ID'})
}
 
};


