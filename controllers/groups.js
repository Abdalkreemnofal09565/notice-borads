const Group = require('../models/group')
const borad = require('../models/borad')
const userBorad = require('../models/user-borad')
const History = require('../models/history')
const history = require('../models/history')


module.exports = {
   
    get: (req, res, next) => {
        Group.get((groups) => {
           
           res.status(200).json({message:'',data: groups})
        })
    },
 
   
    add: (req, res, next) => {
        
            
        Group.findBy('name', req.body.name, (foundGroup) => {
            if (foundGroup){
            res.status(200).json({message:'Group with the same name is already exist'})
            }else{
                userBorad.findBy("borad_id",req.body.boradID,(userBorad) => {
                    if (!userBorad){
                        res.status(200).json({message:'the borad not found'})
                        }else{
                            console.log(userBorad);
                           if(userBorad.user_id== req.userID)
                           {
                            const group = {
                                name: req.body.name,
                                user_borad_id: userBorad.id
                            }
                            Group.add(group, (group) => {
                                Datahistory={
                                    the_id: group.id,
                                    action:"add",
                                    type:"notesGroup",
                                    user_id:req.userID,
                                    
                                  }
                                  History.add(Datahistory,()=>{})
                                res.status(200).json({message:'Group has been created', data:group})
                           })}
                           else{
                               res.status(200).json({message:'you are not added this borad'})
                               }
                       }
                })
          
           }
        })


    },
    addUserToGroup:(req, res, next)=>{
        user.findByID(req.body.user_id,(user)=>{
            if(!user){
                res.status(404).json({message:"the user not found"});

            }else{
                Group.findByID(req.body.group_id,(group)=>{
                    if(!group){
                        res.status(404).json({message:"the group not found"});
        
                    }else{
                        const data={
                            group_id:req.body.group_id,
                            user_id:req.body.user_id
                        }
                        userGroup.add(data,(userGroup)=>{
                             res.status(200).json({message:"the user has added to group"})
                        })
                    }
                })
            }
        })
    }
   ,
   
    delete: (req, res, next) => {
        const id = req.params.id
console.log(id);
Group.findByID(id, (group) => {
            if(!group) {
                res.status(404).json({message:'Group not found'})
        }
            else{
                Group.delete(id, () => {
                    Datahistory={
                        the_id: id,
                        action:"delete",
                        type:"notesGroup",
                        user_id:req.userID,
                        
                      }
                      History.add(Datahistory,()=>{})
                      
                    res.status(200).json({message:'Group has been deleted'})
                })
            }
        })
    }
}

