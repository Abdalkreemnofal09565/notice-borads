const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')

const User = require('../models/user');


exports.signin = (req, res, next) =>  {
        console.log(req);
  const email = req.body.email
  const password = req.body.password

  User.findBy('email', email, (user) => {
    
      if (!user) 
      {
         res.json({message:'User not found'})
      }
      else{
     if(!bcrypt.compareSync(password, user.password))
         res.json({message:'Password is incorrect'})

      let token = jwt.sign({
          id: user.id,
          email: user.email
      }, 'secret')

      res.status(200).json({message:'Login success',data: {token, user}})
    }
  })
};
exports.signup = (req, res, next) =>  {
  let name = req.body.name
  let email = req.body.email
  let password = req.body.password

  

  password = bcrypt.hashSync(password)

  User.findBy('email', email, (user) => {
      if(user)
      {
      res.json({message:'Email is already exist'})
      }
      else{
       
      User.add({name, email, password}, (user) => {
        let token = jwt.sign({
          id: user.id,
          email: user.email
      }, 'secret')
          res.json({message:'user is created', data:{user,token}})
      })
    }

  })
};