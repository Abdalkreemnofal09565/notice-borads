const User = require('../models/user')
const borad = require('../models/borad');
const note = require('../models/note');
const userGroup = require('../models/user-group')
const userBorad =require('../models/user-borad');
const user = require('../models/user');
const group = require('../models/group');
const comment = require('../models/comment');
const choice = require('../models/choices');
const boradRequest = require('../models/boradRequest');
const history = require('../models/history')

module.exports = {
    getUsers: (req, res, next) => {
        User.get((users) => {
          res.status(200).json({message:'',data: users})
        })
    },
    getHistory: (req, res, next) => {
      history.getByUserID(req.userID,(historys)=>{
        console.log(historys)
        if(historys.length){
          var data=new Array()
          var i=0;
         historys.forEach(element => {
           var dataHistory={
            id:element.id,
            action:element.action,
            
           }
           
          User.findByID(element.user_id,(user)=>{
            dataHistory.user=user
            
            if(element.type=="borad")
            {
             borad.findByID(element.the_id,(Borad)=>{
              dataHistory.borad=Borad
              data.push(dataHistory)
              i++
              if(i==historys.length)
              {
                res.status(200).json({data:data})
              }
             })
            }
            if(element.type=="comment")
            {
              comment.findByID(element.the_id,(Comment)=>{
                dataHistory.Comment=Comment
                data.push(dataHistory)
                i++
              if(i==historys.length)
              {
                res.status(200).json({data:data})
              }
             })
            }
            if(element.type=="choices")
            {
              choice.findByID(element.the_id,(choices)=>{
                dataHistory.choices=choices
                data.push(dataHistory)
                i++
              if(i==historys.length)
              {
                res.status(200).json({data:data})
              }
             })
            }
            if(element.type=="notesGroup")
            {
              group.findByID(element.the_id,(notesGroup)=>{
                dataHistory.notesGroup=notesGroup
                data.push(dataHistory)
                i++
              if(i==historys.length)
              {
                res.status(200).json({data:data})
              }
             })
            }
            if(element.type=="notes")
            {
              note.findByID(element.the_id,(notes)=>{
                dataHistory.notes=notes
                data.push(dataHistory)
                i++
              if(i==historys.length)
              {
                res.status(200).json({data:data})
              }
             })
            }
            
            if(element.type=="userGroup")
            {
              userGroup.findByID(element.the_id,(userGroup)=>{
                dataHistory.userGroup=userGroup
                data.push(dataHistory)
                i++
              if(i==historys.length)
              {
                res.status(200).json({data:data})
              }
             })
            }
          })
          
         });
        }else{
         
          res.status(404).json({message:"the history is empty"})
        }
        
      }
      )
    },
    getUser: (req, res, next) => {
        const id = req.params.id

        User.findByID(id, (user) => {
            if (!user){
              res.status(404).json({message:'user not found'})
            }else{
            res.status(200).json({message:'',data: user})
            }
        })
    },
    findUserByName:(req,res,next) => {
      console.log(req.body.name)
      User.getByName(req.body.name,(Users)=>{
        if(!Users){
             res.status(404).json({message:"the user not found "})
        }else{
          res.status(200).json({data:Users})
        }
      })
    }
   
    
}

