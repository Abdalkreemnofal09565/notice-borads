const borad = require('../models/borad');
const note = require('../models/note');
const History = require('../models/history')
const userBorad =require('../models/user-borad');
const user = require('../models/user');
const group = require('../models/group');
const comment = require('../models/comment');
const choice = require('../models/choices');
const boradRequest = require('../models/boradRequest');
exports.getborads = (req, res, next) => {
  
  borad.get().then((borads) => {if(borads){res.status(200).json({message:'',data: borads})}else{res.status(404).json({message:'not found borads'})} })
         
  
};
exports.sendRequest = (req, res, next) => {
  let dataBoradRequest = {
      boradID: req.params.id,
      userID: req.userID
  }

  boradRequest.findBy('userID', dataBoradRequest.userID, (foundBoradRequest) => {
    
      if (foundBoradRequest && foundBoradRequest.BoradID == dataBoradRequest.boradID)
      {
        res.json({message:'You already sent request to this borad'})
      }
      else
      {
          borad.findByID(dataBoradRequest.boradID, (borad) => {
          if (!borad){
              return res.json({message:'borad not found'})
          }else{
            boradRequest.add(dataBoradRequest,borad, (dataBoradResponse) => {
              
           
     
              let userBoradData={
                borad_id: dataBoradResponse.BoradID,
                user_id: dataBoradResponse.userID,
                role:3
              }
              userBorad.add(userBoradData,(userBorad)=>{userBorad})
            
              
              Datahistory={
                the_id:  borad.id,
                action:"sendRequest",
                type:"borad",
                user_id:req.userID
              }
              History.add(Datahistory,()=>{})
         res.status(200).json({message:'Request has been sent', data:dataBoradRequest})
      })
           }
      })
          
  }
})
};
exports.setRequestStatus= (req, res, next) => {
  let id = req.params.requestID
  let accepted = req.body.accepted
  
  boradRequest.findByID(id, (dataBoradRequest) => {
      if(!dataBoradRequest) 
       {
           return res.json({message:'boradRequest not found'})
        
       }else{
       
        boradRequest.edit(id, {accepted},dataBoradRequest, (dataBoradResponse) => {
          if(accepted){
           
     
              let userBoradData={
                borad_id: dataBoradResponse.BoradID,
                user_id: dataBoradResponse.userID,
                role:1
              }
              
              userBorad.findBy('user_id',dataBoradResponse.userID,(userBoradUser_id)=>{
               
                 if(userBoradUser_id)
                 {
                
                 
                    userBorad.findBy('borad_id',userBoradUser_id.borad_id,(userBoradBorad_id)=>{
                      if(userBoradBorad_id)
                      {
                        boradRequest.delete(dataBoradResponse.id,()=>{});
                        userBorad.edit(userBoradBorad_id.id,userBoradData,(userBorad)=>{userBorad})
                         Datahistory={
                          the_id: userBoradUser_id.borad_id,
                          action:"set status request",
                          type:"borad",
                          user_id:req.userID
                        }
                        History.add(Datahistory,()=>{})
                      }
                   })
                   
                  
                 }
              })
             
              
            
        
          }
           res.json({message:`Request has been ${accepted ? 'accepted' : 'rejected'}`})
          
   }) }
})
};
exports.getMyBorads = (req, res, next) => {
  var user_id=req.userID;
 
  userBorad.getByUserID(user_id).then((userBorads) => {
    if(userBorads.length){
      let j = 0
      var data=new Array();
      userBorads.forEach(el_userBorad=>{
        borad.getByBoradID(el_userBorad.borad_id).then((borad)=>{
                    
          j++
          data.push(borad);
              if (j == userBorads.length)
                res.status(200).json({message:"",data:data})
          
        })
     
      })
    } else {
      res.status(404).json({message:"not found borads for you"})
    }
   
  })
         
  
};
exports.getboradDetails = (req, res, next) => {
  data="";
  borad.getBoradsDetails().then((borads) => {
    if(borads)
    {
      
      res.status(200).json({borads})
     
      
    }else{
      res.status(404).json({message:'not found borads'})
    } 
  })
        
  
};
exports.getUserGroupBorads = (req, res, next) => {
 
  borad.getUserGroupBorads(req.params.id).then((borads) => {
    if(borads.length)
    {
      console.log(borads)
      res.status(200).json({data:borads})
     
      
    }else{
      res.status(404).json({message:'not found borads'})
    } 
  })
        
  
};
exports.deleteBorad = (req, res, next) => {
   const id = req.params.BoradID

  borad.findByID(id, (borad) => {
      if(!borad) {
          res.status(404).json({message:'Borad not found'})
      }
      else {
         
              borad.delete(id, () => {
                Datahistory={
                  the_id:  borad.id,
                  action:"delete",
                  type:"borad",
                  user_id:req.userID
                }
                History.add(Datahistory,()=>{})
              
                 res.status(200).json({message:'Borad has been deleted'})
              })
          
        }
  })
  
};

exports.editBorad = (req, res, next) => {
  const id = req.params.id

  const boradData = {
      name: req.body.name,
      type: req.body.type
  }

 
  borad.edit(id, boradData, (borad) => {
      
    
    Datahistory={
      the_id:  borad.id,
      action:"edit",
      action:"borad",
      user_id:req.userID
    }
    History.add(Datahistory,()=>{})
  
          
              res.status(200).json({message:'borad has been updated', data: borad})
          
      })
 
};
exports.addBorad =(req, res, next)=> {
  const boradData = {
      name: req.body.name,
      type: req.body.type,
      group_id:null,
      user_id:req.userID
  }

  borad.add(boradData, (borad) => {
     
      let userBoradData={
        borad_id: borad.id,
        user_id: req.userID,
        role:4
      }
      console.log(userBoradData);
      userBorad.add(userBoradData,(userBorad)=>{userBorad})
      Datahistory={
       the_id:  borad.id,
        action:"add",
        type:"borad",
        user_id:req.userID
      }
      History.add(Datahistory,()=>{})
        res.status(200).json({message:'Borad has been created', data:borad})
    
  })

}
exports.addGroupToNewBorad =(req, res, next)=> {
  const boradData = {
      name: req.body.name,
      type: req.body.type,
      group_id:req.body.group_id,
      user_id:req.userID
  }
  group.findByID(req.body.group_id,(group)=>{
    if(!group){
      res.status(404).json({message:'The group not found'})
    }else{
      borad.add(boradData, (borad) => {
     
        let userBoradData={
          borad_id: borad.id,
          user_id: req.userID
        }
        userBorad.add(userBoradData,(userBorad)=>{userBorad})
        DataHistory={
          the_id:borad.id,
          type:"borad",
          action:"add",
          user_id:req.userID
        }
        History.add(DataHistory,()=>{})
          res.status(200).json({message:'Borad has been created', data:borad})
      
    })
    }
    
  })
  

};
exports.getMemberBorad=(req,res,next)=>{
  userBorad.getMemberBorad(req.params.id,(members)=>{
    if(members){
      var data=new Array();
      var i=0;
      members.forEach(element => {
        var m={}
        user.findByID(element.user_id,(member)=>{
          m.id=member.id;
          m.name=member.name;
          m.email=member.email;
          m.role=element.role;
          i++;
          data.push(m);
          if(i==members.length){
            res.status(200).json({message:"",data:data})
          }
        })
        
      });
      
    }else{
      res.status(404).json({message:"the borad is empty"})
    }
  });
};
exports.removeMember=(req,res,next)=>{
  userBorad.findBy("user_id",req.userID,(isAdmin)=>{
   if(isAdmin.role == 4){
    userBorad.findBy("user_id",req.params.userID,(userBoradUser_id)=>{
      if(userBoradUser_id){
       userBorad.findBy("borad_id",req.params.boradID,(userBoradBorad_id)=>{
         if(userBoradBorad_id){
          userBorad.delete(userBoradBorad_id.id,(deleted)=>{
            Datahistory={
              the_id:  req.params.boradID,
              action:"remove",
              type:"borad",
              user_id:req.userID,
              
            }
            History.add(Datahistory,()=>{})
            res.status(200).json({message:"you are remove this user"})
          })
         }else{
          res.status(404).json({message:"this user not existed in this borad"})
        }
       })
      } else{
        res.status(404).json({message:"this user not existed in this borad"})
      }
     })
   }else if(isAdmin.role==2 ){

  
        userBorad.findBy("user_id",req.params.userID,(userBoradUser_id)=>{
          if(userBoradUser_id){
           userBorad.findBy("borad_id",req.params.boradID,(userBoradBorad_id)=>{
             if(userBoradBorad_id){
               if(userBoradBorad_id.role!=4 && userBoradBorad_id.role!=2 ){
                 
              userBorad.delete(userBoradBorad_id.id,(deleted)=>{
                Datahistory={
                  borad_id:  req.params.boradID,
                  action:"remove",
                  user_id:req.params.userID
                  
                }
                History.add(Datahistory,()=>{})
                res.status(200).json({message:"you are remove this user"})
              })
               }else{
                res.status(422).json({message:"you have not permission "})
               }
             }else{
              res.status(404).json({message:"this user not existed in this borad"})
            }
           })
          } else{
            res.status(404).json({message:"this user not existed in this borad"})
          }
         })
     
   }else{
     res.status(422).json({message:"you have not permission "})
   }
  })
 
};
exports.leaveBorad=(req,res,next)=>{
 
 userBorad.findBy('user_id',req.userID,(userBoradUser_id)=>{
   if(userBoradUser_id){
   userBorad.findBy('borad_id',req.params.boradID,(userBoradBorad_id)=>{
     if(userBoradBorad_id){
           if(userBoradBorad_id.role!=4)
           {
             userBorad.delete(userBoradBorad_id.id,(deleted)=>{
              Datahistory={
                the_id:  req.params.boradID,
                action:"leave",
                type:"borad",
                user_id:req.userID
                
              }
              
              History.add(Datahistory,()=>{})
               res.status(200).json({message:"you are leave this borad"})
             })
           }else{
             res.status(422).json({message:"you are super admin"})
           }
     }else{
       res.status(404).json({message:"you are not joined to this borad"})
     }
   })
  }
  else{
    res.status(404).json({message:"sure if you joined to any borad "})
  }
 })
 };
exports.getborad = (req, res, next) => {
  
  if (req.params.BoradID) {
    
    borad.getBoradDetails(req.params.BoradID).then((borads) => {
      if(borads)
      {
        
        res.status(200).json({borads})
       
        
      }else{
        res.status(404).json({message:'not found borads'})
      } 
    })
   
}
else{
  res.status(404).json({message:'must have the borad ID'})
}
 
};
exports.addBoradInGroup =(req, res, next)=> {
  const boradData = {
      name: req.body.name,
      type: req.body.type,
      group_id:req.params.id,
      user_id:req.userID
  }

  borad.add(boradData, (borad) => {
     
      let userBoradData={
        borad_id: borad.id,
        user_id: req.userID,
        role:4
      }
      console.log(userBoradData);
      userBorad.add(userBoradData,(userBorad)=>{userBorad})
      Datahistory={
        borad_id:  borad.id,
        action:"add borad",
        user_id:req.userID
      }
      History.add(Datahistory,()=>{})
        res.status(200).json({message:'Borad has been created', data:borad})
    
  })

}
exports.getHistory =(req, res, next)=> {
 

  History.get(async (history) => {
    var data=new Array()
     if(history.length){
      for(let i = 0 ; i < history.length; i++){
        var his={}
     borad.findByID(history[i].borad_id,(bor)=>{
           user.findByID(history[i].user_id,(use)=>{
            his.id=history[i].id
            his.action=history[i].action
            his.user=use
            his.borad=bor
            data.push(his)
            if(i==history.length-1){
            
              res.status(200).json({message:"",data:data})
            }
          })
        })
        
  
     
       
       
       
      }
     }else{
       res.status(404).json({message:"the history is empty"})
     }
    
    
  })

}


