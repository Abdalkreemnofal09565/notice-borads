const Group = require('../models/groupUser')
const borad = require('../models/borad')
const GroupRequest = require('../models/groupRequest')
const userGroup = require('../models/user-group')
const user = require('../models/user')
const History = require('../models/history')
const { array } = require('../plugins/multer')



module.exports = {
    get: (req, res, next) => {
        Group.get((groups) => {
           res.status(200).json({message:'',data: groups})
        })
    },
    getMemberGroup: (req, res, next) => {
        console.log(req.params.id);
        userGroup.getMemberGroup(req.params.id,(members)=>{
      if(members){
          console.log(members);
        var data=new Array();
        var i=0;
        members.forEach(element => {
          var m={}
          user.findByID(element.user_id,(member)=>{
            m.id=member.id;
            m.name=member.name;
            m.email=member.email;
            m.role=element.role;
            i++;
            data.push(m);
            if(i==members.length){
              res.status(200).json({message:"",data:data})
            }
          })
          
        });
        
      }else{
        res.status(404).json({message:"the group is empty"})
      }
    });
  },
    getUsers: (req, res, next) => {
        const id = req.params.id

        Group.findByID(id, (group) => {
            if (!group){
                res.status(404).json({message:'Group not found'})
            }else{
            Group.users(id, (users) => {
                res.json({message:'', data:users})
            })
          }
        })
    },
    getRequests: (req, res, next) => {
        let id = req.userID

        GroupRequest.get(groupRequests => {
            groupRequests = groupRequests.filter(groupRequest => groupRequest.groupID == req.params.id && groupRequest.accepted == null )
            var data=new Array()
            var i=0
            groupRequests.forEach(element=>{
              user.findByID(element.userID,(user)=>{
                 var Fdata={request:element,user:user}
                 data.push(Fdata)
                 i++
                 if(groupRequests.length==i)
                 {
                  res.status(200).json({message:'',data: data})
                 }
              })
            })
           
                
            
        })
    },
    sendRequest: (req, res, next) => {
        let groupRequest = {
            groupID: req.params.id,
            userID: req.userID
        }

        GroupRequest.findBy('userID', groupRequest.userID, (foundGroupRequest) => {
            if (foundGroupRequest && foundGroupRequest.groupID == groupRequest.groupID)
            {
              res.json({message:'You already sent request to this group'})
            }
            else
            {
                Group.findByID(groupRequest.groupID, (group) => {
                if (!group){
                    return res.json({message:'Group not found'})
                }else{
              GroupRequest.add(groupRequest,group, (dataGroupResponse) => {
                let userGroupData={
                    borad_id: dataGroupResponse.BoradID,
                    user_id: dataGroupResponse.userID,
                    role:3
                  }
                  userGroup.add(userGroupData,(userGroup)=>{userGroup})
                  Datahistory={
                    the_id:req.params.id,
                    action:"send Request",
                    type:"userGroup",
                    user_id:req.userID,
                    
                  }
                  History.add(Datahistory,()=>{})
               res.status(200).json({message:'Request has been sent', data:groupRequest})
            })
                 }
            })
                
        }
    })
    },
    setRequestStatus: (req, res, next) => {
        let id = req.params.requestID
        let accepted = req.body.accepted
        
        GroupRequest.findByID(id, (dataGroupRequest) => {
            if(!dataGroupRequest) 
             {
                 return res.json({message:'GroupRequest not found'})
              
             }else{
             
                GroupRequest.edit(id, {accepted},dataGroupRequest, (dataGroupResponse) => {
                if(accepted){
                 
           
                    let userGroupData={
                        group_id: dataGroupResponse.GroupID,
                      user_id: dataGroupResponse.userID,
                      role:1
                    }
                    
                    userGroup.findBy('user_id',dataGroupResponse.userID,(userGroupUser_id)=>{
                     
                       if(userGroupUser_id)
                       {
                      
                       
                          userGroup.findBy('group_id',userGroupUser_id.group_id,(userGroupGroup_id)=>{
                            if(userGroupGroup_id)
                            {
                              Datahistory={
                                the_id:req.params.requestID,
                                action:"set Request status",
                                type:"userGroup",
                                user_id:req.userID,
                                
                              }
                              History.add(Datahistory,()=>{})
                              GroupRequest.delete(dataGroupResponse.id,()=>{});
                              userGroup.edit(userGroupGroup_id.id,userGroupData,(userGroup)=>{userGroup})
                             
                            }
                         })
                         
                        
                       }
                    })
                   
                    
                  
              
                }
                 res.json({message:`Request has been ${accepted ? 'accepted' : 'rejected'}`})
                
         }) }
      })
      },
    add: (req, res, next) => {
        const group = {
            name: req.body.name,
            adminID: req.userID,
            type: req.body.type
           
        }

        Group.findBy('name', group.name, (foundGroup) => {
            if (foundGroup)
            {
             res.json({message:'Group with the same name is already exist'})
            }else{
              Group.add(group, (group) => {
                  var userGroupData={
                    group_id: group.id,
                    user_id: req.userID,
                    role:4
                  }
                  userGroup.add(userGroupData,(userGroup)=>{userGroup})
                  Datahistory={
                    the_id:group.id,
                    action:"add",
                    type:"userGroup",
                    user_id:req.userID,
                    
                  }
                  History.add(Datahistory,()=>{})
                res.status(200).json({message:'Group has been created',data: group})
            })
            }
                
                
            
        })


    },
    edit: (req, res, next) => {
        const id = req.params.id

        const group = {
            name: req.body.name,
            users: req.body.users,
        }

      
        Group.findByID(id, (GroupUser) => {
            if(!GroupUser) {
               return res.json({message:'GroupUser not found'})
         }else{
        Group.edit(id, group,GroupUser, (group) => {
           
          

            res.status(200).json({message:'Group has been updated',data: group})
             
            })
        }
    })
        
    },
    delete: (req, res, next) => {
        const id = req.params.id

        group.findByID(id, (group) => {
            if(!group) 
            {
              res.status(404).json({message:'Group not found'})
            }
            else{
              Group.delete(id, () => {
                Datahistory={
                  the_id:id,
                  action:"delete",
                  type:"userGroup",
                  user_id:req.userID,
                  
                }
                History.add(Datahistory,()=>{})
                res.status(200).json({message:'Group has been deleted'})
            })
            }
               
        })
    }
}

