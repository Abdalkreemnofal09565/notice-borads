const path = require('path');


const express = require('express');
const { check } = require('express-validator/check');
const router = express.Router();
const boradController = require('../controllers/borads');

const userController = require('../controllers/users');

const authController = require('../controllers/auth');
const GroupsController = require('../controllers/groups');
const GroupUsersController = require('../controllers/groupUsers');
const NotesController = require('../controllers/notes');
const upload = require('../plugins/multer')
const validation = require('../middleware/validation')

const Auth = require('../middleware/Auth')
const guest = require('../middleware/guest')

router.post('/signup',[check('name').notEmpty(), check('email').notEmpty(), check('password').notEmpty()],validation,authController.signup);
  
  
  router.post('/login',[check('email').notEmpty(), check('password').notEmpty()], validation,authController.signin);
// /add-borad => GET
// router.get('/add-borad', boradController.addborad);



// /borads => GET
router.get('/borads',boradController.getborads);
router.get('/boradDetails', boradController.getboradDetails);
// /borad => GET
router.get('/borad/:BoradID', boradController.getborad);
router.get('/history', boradController.getHistory);
router.post('/my-borads',Auth,boradController.getMyBorads);
// // /add-borad => POST
router.post('/add-borad',Auth,[check('name').notEmpty(), check('type').notEmpty()],validation,boradController.addBorad);
router.post('/add-Group-To-NewBorad', Auth, [check('name').notEmpty(),check("group_id").notEmpty(),check('type').notEmpty()], validation, boradController.addGroupToNewBorad)
// // /Delete-borad => Delete
router.delete('/Delete-borad/:BoradID',Auth,boradController.deleteBorad);

// // /Edit-borad => put
router.put('/Edit-borad/:BoradID',Auth,[check('name').notEmpty()], validation,boradController.editBorad);
router.post('/borad/:id/request', Auth, boradController.sendRequest)
router.post('/borad/request/:requestID', Auth, [check('accepted').notEmpty()], validation, boradController.setRequestStatus)
router.post('/get-member-borad/:id', Auth, boradController.getMemberBorad)
router.post('/leave-borad/:boradID', Auth, boradController.leaveBorad)
router.post('/remove-member/:userID/from-borad/:boradID', Auth, boradController.removeMember)

router.get('/users', userController.getUsers)
router.get('/user/:id', userController.getUser)
router.post("/user/search",[check('name').notEmpty()], validation,userController.findUserByName)
router.post('/user/history', userController.getHistory)
// router.get('/group', GroupsController.get)

// router.get('/group/:id', GroupsController.getOne)

router.get('/groups', GroupsController.get)
router.post('/add-group', Auth, [check('name').notEmpty()], validation, GroupsController.add)
router.post('/add-User-To-Group', Auth, [check('user_id').notEmpty(),check("group_id").notEmpty()], validation, GroupsController.addUserToGroup)
// router.post('/edit-group', Auth, [check('name').notEmpty()], validation, GroupsController.add)
router.delete('/Delete-group/:id', Auth, GroupsController.delete)


router.get('/notes', NotesController.get)
router.post('/add-note',  guest,upload.single('file'),[check('name').notEmpty(),check("group_id").notEmpty()], validation, NotesController.addNote)
router.post('/checkList/name/:id',guest, [check("name").notEmpty()], validation, NotesController.changeNoteCheckList)
router.post('/replace-note',  guest,[check('note_id').notEmpty(),check("group_id").notEmpty()], validation, NotesController.changeNotePlace)
router.post('/note/upload/file',  guest,upload.single('file'),  NotesController.uploadFile)
router.delete('/delete-note/:NoteID',guest,   NotesController.deleteNote)


router.post('/add-comment', guest,upload.single('file'), [check('content').notEmpty(),check("note_id").notEmpty()], validation, NotesController.addComment)
router.delete('/delete-comment/:CommentID', guest, NotesController.deleteComment)

router.post('/add-choice', Auth, [check('name').notEmpty(),check("note_id").notEmpty(),check("status").notEmpty()], validation, NotesController.addChoices)
router.post('/edit-name-choice/:choiceID', Auth, [check('name').notEmpty()], validation, NotesController.editNameChoices)
router.post('/edit-status-choice/:choiceID', Auth, [check("status").notEmpty()], validation, NotesController.editStatusChoices)
router.delete('/delete-choice/:ChoiceID', Auth,  NotesController.deleteChoices)


router.get('/group', GroupUsersController.get)
router.get('/group/requests/:id', Auth, GroupUsersController.getRequests)
router.get('/group/:id/users', Auth, GroupUsersController.getMemberGroup)

router.post('/group/:id/request', Auth, GroupUsersController.sendRequest)
router.post('/group/request/:requestID', Auth, [check('accepted').notEmpty()], validation, GroupUsersController.setRequestStatus)
router.post('/group', Auth, [check('name').notEmpty(),check('type').notEmpty()], validation, GroupUsersController.add)
router.post('/group/add/borad/:id', Auth, [check('name').notEmpty(),check('type').notEmpty()], validation, boradController.addBoradInGroup)
router.get('/group/boradDetails/:id', boradController.getUserGroupBorads);
router.put('/group/:id', Auth, [check('name').notEmpty()], validation, GroupUsersController.edit)
router.delete('/group/:id', Auth, GroupUsersController.delete)
module.exports = router;
