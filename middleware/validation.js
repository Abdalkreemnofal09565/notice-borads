
const { validationResult } = require('express-validator')


module.exports = (req, res, next) => {
    console.log(req.body)
    const errors = validationResult(req)
    
    if (!errors.isEmpty()) {
        // ,field: errors.array()
        res.status(422).json({message:"Invalid input"})
    } else {
        next()
    }
}