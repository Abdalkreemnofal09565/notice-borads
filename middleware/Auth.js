
const jwt = require('jsonwebtoken')
const user = require('../models/user')

module.exports = (req, res, next) => {
    
    if (!req.get('Authorization'))
    {
        
        return res.status(401).json({message:'Unauthenticated'})
    }else{
        console.log('asd')
    let token = req.get('Authorization').split(' ')[1]
    let decodedToken

    try {
        decodedToken = jwt.verify(token, 'secret')
    } catch(e) {
        return res.status(500).json({message:'Invalid token'})
    }

    if (!decodedToken)
    {
    res.status(401).json({message:'Unauthenticated'})
    }else{
    user.findByID(decodedToken.id, (user) => {
        if (!user) 
        {
        return res.status(404).json({message:'User not found'})
        }else{
            next()
        }
    })
  }
 }
}