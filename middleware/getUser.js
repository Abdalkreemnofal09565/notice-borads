
const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
    
    if (!req.get('Authorization')) {return next()
    }else{

    let token = req.get('Authorization').split(' ')[1]
    let decodedToken

    try {
        decodedToken = jwt.verify(token, 'secret')
    } catch(e) {
        return next()
    }

    if (!decodedToken) {
        return next()
       }
        else{

    req.userID = decodedToken.id
    next()
    }
  }
}